FROM fedora:latest

COPY ip_info /
RUN chmod +x /ip_info
RUN mv ip_info /usr/bin/
ENTRYPOINT ["bash", "ip_info"]
